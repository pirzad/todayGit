//
//  main.m
//  todayGit
//
//  Created by Sabri on 9/28/1395 AP.
//  Copyright © 1395 AP AmirAdak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
