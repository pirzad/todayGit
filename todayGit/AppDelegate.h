//
//  AppDelegate.h
//  todayGit
//
//  Created by Sabri on 9/28/1395 AP.
//  Copyright © 1395 AP AmirAdak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

